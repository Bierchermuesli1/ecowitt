build:
	go build -o bin/ecowitt main.go

run:
	go run main.go

compile:
	# raspberry pi 3
	GOOS=linux GOARCH=arm64 GOARM=7 go build -o bin/ecowitt-arm64-7 main.go
	# raspberry pi 4
	GOOS=linux GOARCH=arm64 go build -o bin/ecowitt-arm64-8 main.go
	# MacOS
	GOOS=darwin GOARCH=amd64 go build -o bin/ecowitt-darwin-amd64 main.go
	# Linux
	GOOS=linux GOARCH=amd64 go build -o bin/ecowitt-linux-amd64 main.go
	# OpenBSD
	GOOS=openbsd GOARCH=amd64 go build -o bin/ecowitt-openbsd-amd64 main.go
