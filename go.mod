module gitlab.com/gabeguz/ecowitt

go 1.13

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200511232937-7e40ca221e25 // indirect
	golang.org/x/text v0.3.2 // indirect
)
